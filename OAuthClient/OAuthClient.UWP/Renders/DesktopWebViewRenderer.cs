﻿using OAuthClient.UWP.Renders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(OAuthClient.Renders.DesktopWebView), typeof(DesktopWebViewRenderer))]
namespace OAuthClient.UWP.Renders
{
    public class DesktopWebViewRenderer : WebViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            base.OnElementChanged(e);

            var request = new Windows.Web.Http.HttpRequestMessage(Windows.Web.Http.HttpMethod.Get, new Uri(e.NewElement.Source.GetValue(UrlWebViewSource.UrlProperty).ToString()));
            request.Headers.Add("User-agent", "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10136");
            Control.NavigateWithHttpRequestMessage(request);
        }
    }
}
