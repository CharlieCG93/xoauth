﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace OAuthClient.Tokens
{
    public class Pinterest
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }

        [JsonProperty(PropertyName = "scope")]
        public string[] Scope { get; set; }
    }
}
