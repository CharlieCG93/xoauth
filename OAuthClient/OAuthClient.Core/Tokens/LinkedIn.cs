﻿using Newtonsoft.Json;

namespace OAuthClient.Tokens
{
    public class LinkedIn
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }
        [JsonProperty(PropertyName = "expires_in")]
        public int ExpiresIn { get; set; }
    }
}
