﻿using Newtonsoft.Json;

namespace OAuthClient.Tokens
{
    public class Microsoft
    {
        [JsonProperty(PropertyName = "id_token")]
        public string TokenId { get; set; }

        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }

        [JsonProperty(PropertyName = "expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "scope")]
        public string Scope { get; set; }
    }
}
