﻿using Newtonsoft.Json;
using OAuthClient.Keys;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace OAuthClient.OAuth
{
    public class OAuthOneStandard<T> : OAuthBase<T> where T : new()
    {
        private OAuthOneApiKeys Keys;

        public async override void Init(IApiKeys apiKeys, INavigation navigation, Action<T> callback)
        {
            base.Init(apiKeys, navigation, callback);
            this.Keys = (OAuthOneApiKeys)apiKeys;
            var requestToken = await GetAuthToken();

            var authPage = new WebView {
                Source = string.Format(Settings.OAuthOne.AuthorizeURL[typeof(T).Name], requestToken.OAuthToken)
            };
            authPage.Navigated += AuthPageNavigated;

            await this.Navigation.PushAsync(new ContentPage
            {
                Content = authPage
            });
        }

        private async void AuthPageNavigated(object sender, WebNavigatedEventArgs e)
        {
            if (e.Url.Contains("oauth_token=") && e.Url.Contains("oauth_verifier="))
            {
                await this.Navigation.PopAsync();
                var oauth_token = e.Url.Split('&').FirstOrDefault(s => s.Contains("oauth_token=")).Split('=')[1];
                var oauth_verifier = e.Url.Split('&').FirstOrDefault(s => s.Contains("oauth_verifier=")).Split('=')[1];
                var result = await GetAuthToken(oauth_token, oauth_verifier);
            }
        }

        private string GenerateSignature(string url, Dictionary<string, string> data)
        {
            var headers = string.Join(
                "&",
                data
                    .Union(data)
                    .Select(x => string.Format("{0}={1}", Uri.EscapeDataString(x.Key), Uri.EscapeDataString(x.Value)))
                    .OrderBy(s => s));

            var signatureBase = string.Format(
                "{0}&{1}&{2}",
                "POST",
                Uri.EscapeDataString(url),
                Uri.EscapeDataString(headers.ToString())
            );

            return Convert.ToBase64String(Keys.SignatureHash.ComputeHash(new ASCIIEncoding().GetBytes(signatureBase)));
        }

        private string GenerateOAuthHeader(Dictionary<string, string> data)
        {
            return "OAuth " + string.Join(
                ", ",
                data
                    .Where(x => x.Key.StartsWith("oauth_"))
                    .Select(y => string.Format("{0}=\"{1}\"", Uri.EscapeDataString(y.Key), Uri.EscapeDataString(y.Value)))
                    .OrderBy(w => w)
            );
        }

        private async Task<Global.OAuthTokens> GetAuthToken()
        {
            var headers = new Dictionary<string, string>() {
                {"oauth_callback", Keys.CallbackUri},
                {"oauth_consumer_key",Keys.ConsumerKey},
                {"oauth_token", Keys.AccessToken},
                {"oauth_signature_method","HMAC-SHA1"},
                {"oauth_timestamp", ((int)((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds)).ToString()},
                {"oauth_nonce",  Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture))) },
                {"oauth_version", "1.0"}
            };
            headers.Add("oauth_signature", GenerateSignature(Settings.OAuthOne.RequestTokenURL[typeof(T).Name], headers));
            return await Global.Service.Http().PostAutorizationRequest(Settings.OAuthOne.RequestTokenURL[typeof(T).Name], GenerateOAuthHeader(headers));
        }

        private async Task<Global.OAuthTokens> GetAuthToken(string oauth_token, string oauth_verifier)
        {
            var headers = new Dictionary<string, string>() {
                {"oauth_consumer_key",Keys.ConsumerKey},
                {"oauth_nonce",  Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture))) },
                {"oauth_signature_method","HMAC-SHA1"},
                {"oauth_timestamp", ((int)((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds)).ToString()},
                {"oauth_token", oauth_token },
                {"oauth_version", "1.0"}
            };
            headers.Add("oauth_signature", GenerateSignature(Settings.OAuthOne.RequestTokenURL[typeof(T).Name], headers));
            return await Global.Service.Http().PostAutorizationRequest(Settings.OAuthOne.RequestTokenURL[typeof(T).Name], GenerateOAuthHeader(headers), oauth_verifier);
        }
    }
}

