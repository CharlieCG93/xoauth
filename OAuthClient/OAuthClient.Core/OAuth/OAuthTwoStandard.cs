﻿using OAuthClient.Keys;
using OAuthClient.Renders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace OAuthClient.OAuth
{
    public class OAuthTwoStandard<T> : OAuthBase<T> where T : new()
    {
        private OAuthTwoApiKeys Keys;

        public async override void Init(IApiKeys apiKeys, INavigation navigation, Action<T> callback)
        {
            base.Init(apiKeys, navigation, callback);
            this.Keys = (OAuthTwoApiKeys)apiKeys;

            var authPage = typeof(T).Name != "Google" ? new WebView() : new DesktopWebView();
            authPage.Source = string.Format(Settings.OAuthTwo.AuthURL[typeof(T).Name], Keys.ClientId, Keys.RedirectUri, Keys.Scope);
            authPage.Navigated += AuthPageNavigated;

            await this.Navigation.PushAsync(new ContentPage
            {
                Content = authPage
            });
        }

        private async void AuthPageNavigated(object sender, WebNavigatedEventArgs e)
        {
            if (e.Url.Contains("code="))
            {
                await this.Navigation.PopAsync();
                Keys.Code = e.Url.Split('&').FirstOrDefault(s => s.Contains("code=")).Split('=')[1];
                var result = typeof(T).Name == "Facebook" ?
                    (await Global.Service.Http(OAuthParametersToString()).GetRequestAsync<T>()) :
                    (await Global.Service.Http().PostUrlEncodedRequest<T>(Settings.OAuthTwo.TokenURL[typeof(T).Name], OAuthParametersToList()));

                Callback(result);
            }
        }

        protected virtual List<KeyValuePair<string, string>> OAuthParametersToList()
        {
            return new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("code", Keys.Code),
                new KeyValuePair<string, string>("client_id",Keys.ClientId),
                new KeyValuePair<string, string>("client_secret",Keys.ClientSecret),
                new KeyValuePair<string, string>("redirect_uri",Keys.RedirectUri),
                new KeyValuePair<string, string>("grant_type", "authorization_code")
            };
        }

        protected virtual string OAuthParametersToString()
        {
            return string.Format(Settings.OAuthTwo.TokenURL[typeof(T).Name], Keys.ClientId, Keys.RedirectUri, Keys.ClientSecret, Keys.Code);
        }
    }
}
