﻿using OAuthClient.Keys;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace OAuthClient.OAuth
{
    public interface IOAuth<T>
    {
         void Init(IApiKeys apiKeys, INavigation navigation, Action<T> callback);
    }
}
