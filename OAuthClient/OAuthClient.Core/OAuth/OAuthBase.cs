﻿using OAuthClient.Keys;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace OAuthClient.OAuth
{
    public abstract class OAuthBase<T> : IOAuth<T>
    {
        public INavigation Navigation { get; set; }
        public Action<T> Callback { get; set; }

        public virtual void Init(IApiKeys apiKeys, INavigation navigation, Action<T> callback)
        {
            if (!apiKeys.Validate()) {
                throw new NullReferenceException();
            }
            this.Callback = callback;
            this.Navigation = navigation;
        }
    }
}
