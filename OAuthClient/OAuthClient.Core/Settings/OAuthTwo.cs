﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OAuthClient.Settings
{
    public class OAuthTwo
    {
        public static Dictionary<string, string> AuthURL = new Dictionary<string, string>()
        {
            {"Google", @"https://accounts.google.com/o/oauth2/v2/auth?client_id={0}&redirect_uri={1}&scope={2}&response_type=code" },
            {"LinkedIn", @"https://www.linkedin.com/oauth/v2/authorization?client_id={0}&redirect_uri={1}&state=&scope={2}&response_type=code" },
            {"Facebook", @"https://www.facebook.com/v2.11/dialog/oauth?client_id={0}&redirect_uri={1}&scope={2}&response_type=code" },
            {"Microsoft", @"https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id={0}&redirect_uri={1}&scope={2}&response_type=code" },
            {"Instagram", @"https://api.instagram.com/oauth/authorize/?client_id={0}&redirect_uri={1}&scope={2}&response_type=code" },
            {"Pinterest", @"https://api.pinterest.com/oauth?client_id={0}&redirect_uri={1}&scope={2}&response_type=code&state=768uyFys"}
        };

        public static Dictionary<string, string> TokenURL = new Dictionary<string, string>()
        {
            {"Google", @"https://www.googleapis.com/oauth2/v4/token" },
            {"LinkedIn", @"https://www.linkedin.com/oauth/v2/accessToken" },
            {"Facebook", @"https://graph.facebook.com/v2.11/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}" },
            {"Microsoft", @"https://login.microsoftonline.com/common/oauth2/v2.0/token" },
            {"Instagram", @"https://api.instagram.com/oauth/access_token" },
            {"Pinterest", @"https://api.pinterest.com/v1/oauth/token"}
        };

        public static Dictionary<string, string> Actions = new Dictionary<string, string>()
        {
            {"Google", @"https://www.googleapis.com/plus/v1/people/me?access_token={0}" },
            {"LinkedIn", @"https://www.linkedin.com/v1/people/~?format=json" },
            {"Facebook", @"https://graph.facebook.com/v2.11/me?fields=email,name,id,gender,picture&access_token={0}" },
            {"Microsoft", @"https://graph.microsoft.com/v1.0/me/mailfolders/inbox/messages" },
            {"Instagram", @"https://graph.microsoft.com/v1.0/me/mailfolders/inbox/messages" },
            {"Pinterest", @"https://api.pinterest.com/v1/me/pins?access_token={0}&fields=id,creator,note&limit=1"}
        };
    }
}
