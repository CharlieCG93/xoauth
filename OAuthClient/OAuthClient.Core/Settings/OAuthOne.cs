﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OAuthClient.Settings
{
    public class OAuthOne
    {
        public static Dictionary<string, string> RequestTokenURL = new Dictionary<string, string>()
        {
            {"Twitter", @"https://api.twitter.com/oauth/request_token" }
        };

        public static Dictionary<string, string> AuthorizeURL = new Dictionary<string, string>()
        {
            {"Twitter", @"https://api.twitter.com/oauth/authorize?oauth_token={0}" }
        };
    }
}
