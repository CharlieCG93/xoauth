﻿using OAuthClient.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using OAuthClient.OAuth;

namespace OAuthClient
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            //var client = new OAuthClient.OAuth.OAuthOneStandard("K0lkf9qWoNFrMleKQKyh09HOr", "eHFn5h0HTh4uuRyCnG0ogmhQrjcYpGsdVASpuxh1S4ArdnciNe", "897672854002688000-FiIskKvbnCfadokvFwwd1rdb6oWT2Mk", "HoAezr5jvcMIjfE6pDc227TXLI1BGcXQcfEJfvP9nIZqe", "https://www.getpostman.com/oauth2/callback");
             // await client.Tweet("Hola mundo");
        }

        private void TwitterAuthButtonClicked(object sender, EventArgs e)
        {
            OAuthClient.Global.Service<OAuthOneStandard<Twitter>>.Instance.Init(
            new OAuthClient.Keys.OAuthOneApiKeys("K0lkf9qWoNFrMleKQKyh09HOr", "eHFn5h0HTh4uuRyCnG0ogmhQrjcYpGsdVASpuxh1S4ArdnciNe", "897672854002688000-FiIskKvbnCfadokvFwwd1rdb6oWT2Mk", "HoAezr5jvcMIjfE6pDc227TXLI1BGcXQcfEJfvP9nIZqe", "https://www.getpostman.com/oauth2/callback"),
            this.Navigation,
            (result) =>
            {
                var token = result;
            });
        }

        private void GoogleAuthButtonClicked(object sender, EventArgs e)
        {
            OAuthClient.Global.Service<OAuthTwoStandard<Google>>.Instance.Init(
            new OAuthClient.Keys.OAuthTwoApiKeys("653462684430-8ockoljcgdes9j0jfq1cutnd8vbsdt86.apps.googleusercontent.com", "VL0TMBqzim_idHUkMMViYWxw", "https://github.com/", "openid"),
            this.Navigation,
            (result) =>
            {
                var token = result;
            });
        }

        private void FacebookAuthButtonClicked(object sender, EventArgs e)
        {
            OAuthClient.Global.Service<OAuthTwoStandard<Facebook>>.Instance.Init(
            new OAuthClient.Keys.OAuthTwoApiKeys("1930187370331591", "83dfe4a60bda61705e9c59f3f879d498", "https://www.getpostman.com/oauth2/callback", "openid"),
            this.Navigation,
            (result) =>
            {
                var token = result;
            });
        }

        private void LinkedInAuthButtonClicked(object sender, EventArgs e)
        {
           OAuthClient.Global.Service<OAuthTwoStandard<LinkedIn>>.Instance.Init(
           new OAuthClient.Keys.OAuthTwoApiKeys("78cba8rhl4e8on", "s9tuGRm2wYx53gG9", "https://www.getpostman.com/oauth2/callback"),
           this.Navigation,
           (result) =>
           {
               var token = result;
           });
        }

        private void OutlookAuthButtonClicked(object sender, EventArgs e)
        {
            OAuthClient.Global.Service<OAuthTwoStandard<OAuthClient.Tokens.Microsoft>>.Instance.Init(
            new OAuthClient.Keys.OAuthTwoApiKeys("74af65bd-5bfa-42f0-bbaa-ab715ca09a0c", "kaiGWQF6173?bolyREN2~>{", "https://www.getpostman.com/oauth2/callback", "openid+Mail.Read"),
            this.Navigation,
            (result) =>
            {
                var token = result;
            });
        }

        private void InstagramAuthButtonClicked(object sender, EventArgs e)
        {
            OAuthClient.Global.Service<OAuthTwoStandard<Instagram>>.Instance.Init(
            new OAuthClient.Keys.OAuthTwoApiKeys("06e32484e39f453b8377fada10a2dbb4", "4ab4bfd71b454d849ba0b3f464b70785", "https://www.getpostman.com/oauth2/callback", "basic"),
            this.Navigation,
            (result) =>
            {
                var token = result;
            });
        }

        private void PinterestAuthButtonClicked(object sender, EventArgs e)
        {
            OAuthClient.Global.Service<OAuthTwoStandard<Pinterest>>.Instance.Init(
            new OAuthClient.Keys.OAuthTwoApiKeys("4934820951117282676", "fd74f55d82169d059bd3e9015a42ce741ea95ea91bac6e7b8d74363d56cd659d", "https://www.getpostman.com/oauth2/callback", "read_public,write_public"),
            this.Navigation,
            (result) =>
            {
                var token = result;
            });
        }
    }
}
