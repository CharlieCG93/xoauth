﻿using Newtonsoft.Json;
using OAuthClient.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OAuthClient.Helpers
{
    public class HttpHelper : HttpClient
    {
        public string ServiceRoot { get; set; }

        #region General HTTP requests
        public async Task<List<T>> RetriveRequestAsync<T>(string endpoint)
        {

            return await Generics.TryCatch(async () =>
            {
                var response = await this.GetAsync(UrlBuilder(ServiceRoot, endpoint));
                return response.IsSuccessStatusCode ?
                        JsonConvert.DeserializeObject<List<T>>((await response.Content.ReadAsStringAsync())) :
                        new List<T>();
            });
        }

        public async Task<T> GetRequestAsync<T>(string endpoint = "", string identifier = "") where T : new()
        {
            return await Generics.TryCatch(async () =>
            {
                var response = await this.GetAsync(UrlBuilder(ServiceRoot, endpoint, identifier));
                return response.IsSuccessStatusCode ?
                    JsonConvert.DeserializeObject<T>((await response.Content.ReadAsStringAsync())) :
                    new T();
            });
        }

        public async Task<bool> PostRequestAsync<T>(T data, string endpoint)
        {
            return await Generics.TryCatch(async () =>
            {
                HttpResponseMessage response = await this.PostAsync(UrlBuilder(ServiceRoot, endpoint), new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
                return response.IsSuccessStatusCode ? true : false;
            });
        }

        public async Task<bool> EditRequestAsync<T>(T data, string endpoint, string identifier = "")
        {
            return await Generics.TryCatch(async () =>
            {
                HttpResponseMessage response = await this.PutAsync(UrlBuilder(ServiceRoot, endpoint, identifier), new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
                return response.IsSuccessStatusCode ? true : false;
            });
        }

        public async Task<bool> DeleteRequestAsync(string endpoint, string identifier = "")
        {
            return await Generics.TryCatch(async () =>
            {
                HttpResponseMessage response = await this.DeleteAsync(UrlBuilder(ServiceRoot, endpoint, identifier));
                return response.IsSuccessStatusCode ? true : false;
            });
        }

        public async Task<T> PostUrlEncodedRequest<T>(string endpoint, IEnumerable<KeyValuePair<string, string>> postData) where T : new()
        {
            return await Generics.TryCatch(async () =>
            {
                var response = await this.SendAsync(new HttpRequestMessage(HttpMethod.Post, endpoint)
                {
                    Content = new FormUrlEncodedContent(postData)
                });
                var a = await response.Content.ReadAsStringAsync();
                return response.IsSuccessStatusCode ?
                    JsonConvert.DeserializeObject<T>((await response.Content.ReadAsStringAsync())) :
                    new T();
            });
        }


        public async Task<Global.OAuthTokens> PostAutorizationRequest(string endpoint, string authorization, string body = null)
        {
            return await Generics.TryCatch(async () =>
            {
                this.DefaultRequestHeaders.Add("Authorization", authorization);

                var response = body != null ? await this.PostAsync(endpoint, new StringContent("oauth_verifier="+body)) : await this.PostAsync(endpoint, null);
                
                return response.IsSuccessStatusCode ?
                    Global.Statics.ParseQueryString((await response.Content.ReadAsStringAsync())):
                    new OAuthTokens();
            });
        }

        #endregion

        public Uri UrlBuilder(params string[] parameters)
        {
            return new Uri(string.Join("/", parameters.Where(x => x != "")));
        }
    }
}
