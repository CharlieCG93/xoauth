﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OAuthClient.Global
{
    public class Statics
    {
        public static OAuthTokens ParseQueryString(string queryString)
        {
            return new OAuthTokens
            {
                OAuthToken = queryString.Split('&')[0].Split('=')[1],
                OAuthTokenSecret = queryString.Split('&')[1].Split('=')[1],
                CallbackConfirm = queryString.Split('&')[2].Split('=')[1]
            };
        }
    }

    public class OAuthTokens
    {
        [JsonProperty(PropertyName = "oauth_token")]
        public string OAuthToken { get; set; }
        [JsonProperty(PropertyName = "oauth_token_secret")]
        public string OAuthTokenSecret { get; set; }
        [JsonProperty(PropertyName = "oauth_callback_confirmed")]
        public string CallbackConfirm { get; set; }
    }
}
