﻿using OAuthClient.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace OAuthClient.Global
{
    public class Service
    {
        private static HttpHelper http;

        public static HttpHelper Http(string serviceRoot = "")
        {
            if (http == null)
            {
                http = new HttpHelper();
            }
            http.ServiceRoot = serviceRoot;
            return http;
        }
    }

    public abstract class Service<T> where T : class
    {
        private static readonly Lazy<T> instance = new Lazy<T>(CreateTemplateInstance, LazyThreadSafetyMode.ExecutionAndPublication);

        #region Properties
        public static T Instance
        {
            get { return instance.Value; }
        }
        #endregion

        #region Methods
        private static T CreateTemplateInstance()
        {
            return Activator.CreateInstance(typeof(T)) as T;
        }
        #endregion
    }
}
