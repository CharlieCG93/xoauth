﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace OAuthClient.Global
{
    public class Generics
    {
        public static async Task<TResult> TryCatch<TResult>(Func<Task<TResult>> callback) where TResult : new()
        {
            try
            {
                var result = await callback();
                return result;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return new TResult();
        }
    }
}
