﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace OAuthClient.Keys
{
    public class OAuthTwoApiKeys : IApiKeys
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Scope { get; set; }
        public string RedirectUri { get; set; }
        public string Code { get; set; }

        public OAuthTwoApiKeys(string clientId, string clientSecret, string redirecUri, string scope= "")
        {
            this.ClientId = clientId;
            this.ClientSecret = clientSecret;
            this.Scope = scope;
            this.RedirectUri = redirecUri;
        }

        public bool Validate()
        {
            return true;
        }
    }
}
