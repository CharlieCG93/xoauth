﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OAuthClient.Keys
{
    public interface IApiKeys
    {
        bool Validate();
    }
}
