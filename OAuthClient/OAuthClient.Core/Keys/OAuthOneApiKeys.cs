﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace OAuthClient.Keys
{
    public class OAuthOneApiKeys : IApiKeys
    {
        public string ConsumerKey { get; set; }
        public string ConsumerKeySecret { get; set; }
        public string AccessToken { get; set; }
        public string AccessTokenSecret { get; set; }
        public string CallbackUri { get; set; }
        public HMACSHA1 SignatureHash { get; set; }
        public string Code { get; set; }
        public OAuthOneApiKeys(string consumerKey, string consumerKeySecret, string accessToken, string accessTokenSecret, string callbackUri)
        {
            this.ConsumerKey = consumerKey;
            this.ConsumerKeySecret = consumerKeySecret;
            this.AccessToken = accessToken;
            this.AccessTokenSecret = accessTokenSecret;
            this.CallbackUri = callbackUri;
            this.SignatureHash = new HMACSHA1(new ASCIIEncoding().GetBytes(string.Format("{0}&{1}", consumerKeySecret, accessTokenSecret)));
        }

        public bool Validate()
        {
            return true;
        }
    }
}
